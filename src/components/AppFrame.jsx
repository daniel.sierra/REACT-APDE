import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { FaAmazon } from "react-icons/fa";
import { Grid, Row, Col } from 'react-flexbox-grid';

const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};


export default class AppFrame extends Component {

  render() {
    return (
      <div className={styles.root}>

        <AppBar position="static">
          <Toolbar>
            <Row>

              <Col xs>
                <IconButton className={styles.menuButton} color="inherit" aria-label="Menu">
                  <FaAmazon />
                </IconButton>
              </Col>

              <Col xs>
                <Typography variant="title" color="inherit" className={styles.flex}>
                  APDE DEMO
                </Typography>
              </Col>

            </Row>
          </Toolbar>

        </AppBar>

      </div>
    )
  }

}
